# iochat

Minimalist FIFO and filesystem-based text chat client, similar to [ii](tools.suckless.org/ii/)

Currently only supports discord (provided a token), but IRC, Matrix, and possibly Signal protocols will be added in the future.

## Usage

iochat uses strictly IO for communication with a service.. as the name implies..

It takes no arguments at the command line, but instead reads a config.ini file

`./iochat`

### Config

(Read iochat.example.ini)

To enable a service, create the [header], with the "enabled" element set to yes.

For Discord, pass token into the "token" element.

### Structure

The file structure is very simple:

\<outputdir\>/\<client\>/\<channel/room (optionally a server:channel/room)\>/{in,out}

For discord, you need to enter the \<outputdir\>, the next level is the client, "discord", followed by the name of the server the channel is in, then the name of the channel.

### Reading Chat

Since everything is raw text, the "in" file can just be catted or manipulated however you see fit.

`cat ~/.local/share/iochat/discord/mycoolepicserver/general/in`

### Sending Messages

Once again, everything is raw text, therefore you simply just need to put text into the "out" file, and it well be sent and the file will be cleared for the next message.

`echo hello! > ~/.local/share/iochat/discord/mycoolepicserver/general/out`
