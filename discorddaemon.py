import common
import discord
import os
from watchgod import awatch

client = discord.Client()
channeldict = dict()

@client.event
async def on_ready():
    for guild in client.guilds:
        chnls = []
        for channel in guild.channels:
            if channel.type == discord.ChannelType.text:
                channeldict[f"{guild.name}{channel.name}"] = channel.id
                chnls.append(channel.name)

        common.mkdirs(f"discord/{guild.name}", chnls) 

    async for changes in awatch(os.path.expanduser(f"{common.outputdir}discord/")):
        for change in changes:
            path = change[1]
            split = path.split("/")
            split.reverse() # Reverse returns None so it has to be on a separate line smh
            if split[0] == "out":
                channel = client.get_channel(channeldict[f"{split[2]}{split[1]}"])
                with open(path, "r+") as f:
                    msg = f.read()
                    if msg:
                        await channel.send(msg)
                        f.truncate(0)
                    f.close()


@client.event
async def on_message(message):
    # :troll:
    async for msg in message.channel.history(limit=1):
        if msg.guild != None:
            common.writeinfile(f"discord/{msg.guild.name}", msg.channel.name, f"{msg.author.name}: {msg.content}\n")

def discord_run(conf):
    client.run(conf['discord']['token'], bot=False)
