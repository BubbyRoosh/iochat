from enum import Enum
import os

outputdir = '~/.local/share/iochat/'

def writeinfile(client, channel, msg):
    infilename = os.path.expanduser(f"{outputdir}{client}/{channel}/in")
    try:
        infile = open(infilename, "a")
        infile.writelines(msg)
        infile.close()
    except:
        print_error(ErrorType.FileIOError, "writeinfile", f"{infilename} does not exist or cannot be accessed")

def mkdirs(client, channels):
    "Creates the given directories for the channels of a client"
    for channel in channels:
        channeldir = os.path.expanduser(f"{outputdir}{client}/{channel}")
        os.makedirs(channeldir, exist_ok=True)
        # Open like this with .close() (GC could do it too) creates the file if it doesn't exist
        open(f"{channeldir}/in", "a").close()
        open(f"{channeldir}/out", "a").close()

class ErrorType(Enum):
    FileIOError = 0

def print_error(err, src, msg):
    print(f"{err.name} received from {src}: {msg}")
